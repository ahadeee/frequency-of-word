#!/usr/bin/env python
# coding: utf-8

#                                        Md Abdul Ahad
#                                       ahadeee@gmail.com

# In[23]:


import pandas as pd


# In[25]:


ls


# In[26]:


df1=pd.read_csv("ah1.txt")
df2=pd.read_csv("ah2.txt")
df3=pd.read_csv("ah3.txt")


# In[27]:


frames=[df1,df2,df3]


# In[28]:


result=pd.concat(frames)


# In[29]:


result.to_csv('ahad.txt',encoding='utf-8',index = False)


# In[30]:


ls


# In[31]:


v=open("ahad.txt","r")


# In[32]:


m={}
for t in v:
    t=t.strip()
    t=t.lower()
    p=t.split(" ")
    for q in p:
        if q in m:
            m[q] =m[q]+1
        else:
                    m[q]=1
print(m)


# In[33]:


import datetime


# In[34]:


now=datetime.datetime.now()


# In[35]:


print("Current date and time : ")
print(now.strftime("%Y.%m.%d %H:%M:%S"))


# In[ ]:




